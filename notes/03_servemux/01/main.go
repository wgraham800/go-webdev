package main

import (
	"io"
	"net/http"
)

type dog int
type cat int

func (m dog) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	_, _ = io.WriteString(res, "This is a doggy dog")
}

func (m cat) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	_, _ = io.WriteString(res, "This is a kitty cat")
}

func main() {
	var d dog
	var c cat

	mux := http.NewServeMux()
	mux.Handle("ak.rocks/", d)
	mux.Handle("mysite.com/", c)

	_ = http.ListenAndServe(":80", mux)
}
