package main

import (
	"io"
	"net/http"
)

type dog int
type cat int

func d(res http.ResponseWriter, req *http.Request) {
	_, _ = io.WriteString(res, "This is a doggy dog")
}

func c(res http.ResponseWriter, req *http.Request) {
	_, _ = io.WriteString(res, "This is a kitty cat")
}

func main() {
	http.HandleFunc("ak.rocks/", d)
	http.HandleFunc("mysite.com/", c)

	_ = http.ListenAndServe(":80", nil)
}
