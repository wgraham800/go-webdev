package main

import (
  "net/http"
  "fmt"
)

type hotdog int

func (m hotdog) ServeHTTP(w http.ResponseWriter, r *http.Request){
  fmt.Println(w, "Any code or response you want goes here")
}

func main(){
  var d hotdog
  http.ListenAndServe(":8080", d)
}
