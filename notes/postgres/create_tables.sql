CREATE TABLE employees (
   ID       SERIAL PRIMARY KEY NOT NULL,
   NAME     TEXT    NOT NULL,
   SCORE    INT     DEFAULT 10 NOT NULL,
   SALARY   REAL
);

CREATE TABLE phonenumbers (
   ID       SERIAL PRIMARY KEY NOT NULL,
   PHONE    TEXT NOT NULL,
   EMP_ID   INT      references employees(ID)
);

CREATE TABLE person (
   ID       SERIAL PRIMARY KEY NOT NULL,
   NAME     TEXT NOT NULL
);

CREATE TABLE sport (
   ID       SERIAL PRIMARY KEY NOT NULL,
   NAME     TEXT NOT NULL,
   P_ID     INT      references person(ID)
);

INSERT INTO employees (NAME,SCORE,SALARY) VALUES 
    ('Daniel', 23, 55000.00), 
    ('Arin', 25, 65000.00), 
    ('Juan', 24, 72000.00), 
    ('Shen', 26, 64000.00), 
    ('Myke', 27, 58000.00), 
    ('McLeod', 26, 72000.00), 
    ('James', 32, 35000.00);

INSERT INTO phonenumbers (PHONE,EMP_ID) VALUES 
    ('555-777-8888', 4), 
    ('555-222-3345', 4), 
    ('777-543-3451', 1), 
    ('544-756-2334', 2);

INSERT INTO person (NAME) VALUES 
    ('Shen'), 
    ('Daniel'), 
    ('Juan'), 
    ('Arin'), 
    ('McLeod');

INSERT INTO sport (NAME, P_ID) VALUES 
    ('Surf',1),
    ('Soccer',3),
    ('Ski',3),
    ('Sail',3),
    ('Bike',3);
