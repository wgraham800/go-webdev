-- Inner Join between employees pkey ID with phonenumbers fkey emp_id
SELECT employees.NAME, phonenumbers.PHONE FROM 
    employees INNER JOIN phonenumbers 
    ON employees.ID = phonenumbers.EMP_ID;

-- Cross Join = Cartesian product  - lots of data
SELECT * from employees CROSS JOIN phonenumbers;
SELECT person.NAME, sport.NAME FROM person CROSS JOIN sport;
