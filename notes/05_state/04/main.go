package main

import (
	"html/template"
	"net/http"
)

var tpl *template.Template

type Person struct {
	FirstName  string
	LastName   string
	Subscribed bool
}

func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))
}

func main() {
	http.HandleFunc("/", foo)
	http.ListenAndServe(":8080", nil)
}

func foo(w http.ResponseWriter, req *http.Request) {

	f := req.FormValue("firstName")
	l := req.FormValue("lastName")
	s := req.FormValue("subscribe") == "on"

	tpl.ExecuteTemplate(w, "index.gohtml", Person{f, l, s})
}
