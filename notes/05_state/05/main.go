package main

import (
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
)

func main() {
	http.HandleFunc("/", foo)
	http.ListenAndServe(":8080", nil)
}

func foo(w http.ResponseWriter, req *http.Request) {
	var s string
	var nf *os.File
	var err error

	if req.Method == http.MethodPost {
		// Read uploaded file and convert to string bs
		file, header, _ := req.FormFile("file")
		defer file.Close()

		bs, _ := ioutil.ReadAll(file)
		s = string(bs)

		nf, err = openOrCreateFile(header.Filename)
		if err != nil {
			log.Fatalln(err)
		}
		defer nf.Close()

		fmt.Println(s)
		_, err = nf.Write(bs)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	tpl, err := template.ParseGlob("*.gohtml")
	if err != nil {
		fmt.Println("Something terrible happened")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	tpl.ExecuteTemplate(w, "index.gohtml", s)
	io.WriteString(w, "What the fuck is goiung on")
}

func openOrCreateFile(filename string) (*os.File, error) {
	var err error
	var nf *os.File

	_, err = os.Stat("uploaded/" + filename)
	if err != nil {
		fmt.Println("File already exists")
		nf, err = os.Open("uploaded/" + filename)
		if err != nil {
			return nil, err
		}
		return nf, nil
	}
	fmt.Printf("Attempting to create file: uploaded/%s", filename)
	nf, err = os.Create(filepath.Join("uploaded/", filename))
	if err != nil {
		return nil, err
	}
	return nf, nil

}
