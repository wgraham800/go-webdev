package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/", set)
	http.HandleFunc("/cookies", abundance)
	http.HandleFunc("/read", read)
	http.ListenAndServe(":8080", nil)
}

func set(w http.ResponseWriter, req *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:  "my-cookie",
		Value: "some value",
	})
	fmt.Print("Wrote a cookie!")
}

func abundance(w http.ResponseWriter, req *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:  "my-cookie",
		Value: "some value",
	})
}

func read(w http.ResponseWriter, req *http.Request) {
	cookies := req.Cookies()
	if len(cookies) == 0 {
		return
	} else {
		fmt.Println("Here are all your cookies!!")
		for i, cookie := range cookies {
			fmt.Printf("Cookie #%d\nName:%s\nValue:%s\n\n", i, cookie.Name, cookie.Value)
		}
	}
}
