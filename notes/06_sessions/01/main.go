package main

import (
	"fmt"
	"net/http"

	"github.com/google/uuid"
)

var id uuid.UUID

func main() {
	http.HandleFunc("/", set)
	http.ListenAndServe(":8080", nil)
}

func set(w http.ResponseWriter, req *http.Request) {
	c, err := req.Cookie("session")
	if err != nil {
		id = uuid.New()
		c = &http.Cookie{
			Name:  "session",
			Value: id.String(),
		}
		http.SetCookie(w, c)
	}
	fmt.Println("Wrote a cookie!", c)
}
