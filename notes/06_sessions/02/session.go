package main

import (
	"net/http"
)

func loggdIn(req *http.Request) bool {
	c, err := req.Cookie("session")
	if err != nil {
		return false
	}
	uname := dbSessions[c.Value]
	_, ok := dbUsers[uname]
	return ok
}

func getUser(req *http.Request) user {
	var u user

	c, err := req.Cookie("session")
	if err != nil {
		return u
	}
	if uname, ok := dbSessions[c.Value]; ok {
		u = dbUsers[uname]
	}
	return u
}
