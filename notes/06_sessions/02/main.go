package main

import (
	"html/template"
	"net/http"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

type user struct {
	Username, First, Last, Role string
	PasswordHash                []byte
}

var tpl *template.Template
var u user
var dbUsers = map[string]user{}      // map user ID to user
var dbSessions = map[string]string{} // map session ID to user ID

func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))
	bs, _ := bcrypt.GenerateFromPassword([]byte("password"), bcrypt.MinCost)
	dbUsers["test@test.com"] = user{"test@test.com", "Bond", "James", "007", bs}
}

func main() {
	http.HandleFunc("/", index)
	http.HandleFunc("/bar", bar)
	http.HandleFunc("/register", register)
	http.HandleFunc("/login", login)
	http.HandleFunc("/logout", logout)
	http.Handle("/favicon.ico", http.NotFoundHandler())
	http.ListenAndServe(":8080", nil)
}

func index(w http.ResponseWriter, req *http.Request) {
	u := getUser(req)
	tpl.ExecuteTemplate(w, "index.gohtml", u)
}

func bar(w http.ResponseWriter, req *http.Request) {
	u := getUser(req)
	if !loggdIn(req) {
		http.Redirect(w, req, "/", http.StatusSeeOther)
		return
	}
	if u.Role != "007" {
		http.Error(w, "You must be a secret agent to enter the bar", http.StatusForbidden)
		return
	}

	tpl.ExecuteTemplate(w, "bar.gohtml", u)
}

func register(w http.ResponseWriter, req *http.Request) {
	if loggdIn(req) {
		http.Redirect(w, req, "/", http.StatusSeeOther)
		return
	}

	// process form submission
	if req.Method == http.MethodPost {
		uname := req.FormValue("username")
		pass := req.FormValue("password")
		f := req.FormValue("firstname")
		l := req.FormValue("lastname")
		r := req.FormValue("role")

		// Check if username already exists
		if _, ok := dbUsers[uname]; ok {
			http.Error(w, "Username already taken", http.StatusForbidden)
			return
		}

		// Set session cookie
		c := &http.Cookie{
			Name:  "session",
			Value: uuid.New().String(),
		}
		http.SetCookie(w, c)
		dbSessions[c.Value] = uname

		hashedPass, err := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
		u = user{
			Username:     uname,
			PasswordHash: hashedPass,
			First:        f,
			Last:         l,
			Role:         r,
		}

		dbUsers[uname] = u

		http.Redirect(w, req, "/", http.StatusSeeOther)
		return
	}

	tpl.ExecuteTemplate(w, "register.gohtml", u)
}

func login(w http.ResponseWriter, req *http.Request) {
	if loggdIn(req) {
		http.Redirect(w, req, "/", http.StatusSeeOther)
		return
	}

	// process form submission
	if req.Method == http.MethodPost {
		uname := req.FormValue("username")
		pass := req.FormValue("password")

		u, ok := dbUsers[uname]
		if !ok {
			http.Error(w, "Username or password is incorrect", http.StatusInternalServerError)
			return
		}
		err := bcrypt.CompareHashAndPassword(u.PasswordHash, []byte(pass))
		if err != nil {
			http.Error(w, "Username or password is incorrect", http.StatusInternalServerError)
			return
		}

		// Set session cookie
		c := &http.Cookie{
			Name:  "session",
			Value: uuid.New().String(),
		}
		http.SetCookie(w, c)
		dbSessions[c.Value] = uname
		http.Redirect(w, req, "/", http.StatusSeeOther)
		return
	}

	tpl.ExecuteTemplate(w, "login.gohtml", nil)
}

func logout(w http.ResponseWriter, req *http.Request) {
	if loggdIn(req) {
		c, _ := req.Cookie("session")
		delete(dbSessions, c.Value)

		c.MaxAge = -1
		c.Value = ""
		http.SetCookie(w, c)
	}
	http.Redirect(w, req, "/", http.StatusSeeOther)
}
