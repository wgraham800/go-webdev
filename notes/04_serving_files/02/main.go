package main

import (
	"io"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/", dog)
	http.HandleFunc("/my_name_andrew.jpeg", dogCopy)
	http.ListenAndServe(":8080", nil)
}

func dog(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	io.WriteString(w, `<img src="my_name_andrew.jpeg">`)
}

func dogCopy(w http.ResponseWriter, req *http.Request) {
	f, _ := os.Open("my_name_andrew.jpeg")
	defer f.Close()
	io.Copy(w, f)
}
