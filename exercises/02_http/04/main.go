package main

import (
	"fmt"
	"net/http"
)

type dog int

func (m dog) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Will-key", "This is from you!")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	fmt.Println(w, "<h1>Any code you want in this func</h1>")
}

func main() {
	var d dog
	_ = http.ListenAndServe(":8080", d)
}
