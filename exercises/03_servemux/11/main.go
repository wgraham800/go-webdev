package main

import (
	"bufio"
	"fmt"
	"io"
	"net"
	"strings"
)

func serve(c net.Conn) {
	defer c.Close()
	s := bufio.NewScanner(c)
	var reqMethod, reqUri, response string

	i := 0
	for s.Scan() {

		ln := s.Text()
		fmt.Println(ln)

		if i == 0 {
			reqMethod = strings.Fields(ln)[0]
			reqUri = strings.Fields(ln)[1]
		}
		if ln == "" {
			break
		}
		i++
	}

	switch {
	case reqMethod == "GET" && reqUri == "/":
		response = handleIndex(c)
	case reqMethod == "GET" && reqUri == "/apply":
		response = handleApply(c)
	case reqMethod == "POST" && reqUri == "/apply":
		response = handleApplyPost(c)
	default:
		response = handleDefault(c)
	}

	io.WriteString(c, "HTTP/1.1 200 OK\r\n")
	fmt.Fprintf(c, "Content-Length: %d\r\n", len(response))
	fmt.Fprint(c, "Content-Type: text/html\r\n")
	io.WriteString(c, "\r\n")
	io.WriteString(c, response)
}

func handleApply(c net.Conn) string {
	return `
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<title>GET SUM</title>
		</head>
		<body>
			<h1>"GET APPLY"</h1>
			<a href="/">index</a><br>
			<a href="/apply">apply</a><br>
			<form action="/apply" method="POST">
			<input type="hidden" value="In my good death">
			<input type="submit" value="submit">
			</form>
		</body>
		</html>
	`
}

func handleApplyPost(c net.Conn) string {
	return `
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<title>POST APPLY</title>
		</head>
		<body>
			<h1>"POST APPLY"</h1>
			<a href="/">index</a><br>
			<a href="/apply">apply</a><br>
		</body>
	</html>
	`
}

func handleDefault(c net.Conn) string {
	return `
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<title>default</title>
		</head>
		<body>
			<h1>"default"</h1>
		</body>
		</html>
	`
}

func handleIndex(c net.Conn) string {
	return `
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<title>GET INDEX</title>
		</head>
		<body>
			<h1>"GET INDEX"</h1>
			<a href="/">index</a><br>
			<a href="/apply">apply</a><br>
		</body>
		</html>
	`
}

func main() {
	li, _ := net.Listen("tcp", ":8080")
	defer li.Close()

	for {
		conn, _ := li.Accept()
		go serve(conn)
	}
}
