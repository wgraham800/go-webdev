package main

import (
	"html/template"
	"io"
	"net/http"
)

func routeIndex(w http.ResponseWriter, r *http.Request) {
	_, _ = io.WriteString(w, "This is the index route")
}

func routeMe(w http.ResponseWriter, r *http.Request) {
	_ = tpl.ExecuteTemplate(w, "dog.gohtml", "Will")
}

func routeDog(w http.ResponseWriter, r *http.Request) {
	_, _ = io.WriteString(w, "This is the dog route")
}

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("dog.gohtml"))
}

func main() {
	http.HandleFunc("/", routeIndex)
	http.HandleFunc("/dog/", routeDog)
	http.HandleFunc("/me/", routeMe)
	_ = http.ListenAndServe(":8080", nil)
}
