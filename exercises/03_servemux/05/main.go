package main

import (
	"bufio"
	"fmt"
	"io"
	"net"
)

func main() {
	li, _ := net.Listen("tcp", ":8080")
	defer li.Close()

	for {
		conn, _ := li.Accept()

		go func(c net.Conn) {
			defer c.Close()

			s := bufio.NewScanner(c)
			for s.Scan() {
				fmt.Println(s.Text())
			}

			fmt.Println("Code got here.")
			io.WriteString(c, "I see you connected.")
		}(conn)
	}
}
