package main

import (
	"bufio"
	"fmt"
	"io"
	"net"
)

func serve(c net.Conn) {
	defer c.Close()
	s := bufio.NewScanner(c)
	for s.Scan() {
		ln := s.Text()
		fmt.Println(ln)
		if ln == "" {
			break
		}
	}
	io.WriteString(c, "I see you connected.")
}

func main() {
	li, _ := net.Listen("tcp", ":8080")
	defer li.Close()

	for {
		conn, _ := li.Accept()
		go serve(conn)
	}
}
