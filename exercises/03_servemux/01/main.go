package main

import (
	"io"
	"net/http"
)

func routeIndex(w http.ResponseWriter, r *http.Request) {
	_, _ = io.WriteString(w, "This is the index route")
}

func routeMe(w http.ResponseWriter, r *http.Request) {
	_, _ = io.WriteString(w, "Ma nom Will")
}

func routeDog(w http.ResponseWriter, r *http.Request) {
	_, _ = io.WriteString(w, "This is the dog route")
}

func main() {
	http.HandleFunc("/", routeIndex)
	http.HandleFunc("/dog/", routeDog)
	http.HandleFunc("/me/", routeMe)
	_ = http.ListenAndServe(":8080", nil)
}
