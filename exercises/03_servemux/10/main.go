package main

import (
	"bufio"
	"fmt"
	"io"
	"net"
	"strings"
)

func serve(c net.Conn) {
	defer c.Close()
	s := bufio.NewScanner(c)

	i := 0
	for s.Scan() {

		ln := s.Text()
		fmt.Println(ln)

		if i == 0 {
			reqMethod := strings.Fields(ln)[0]
			reqUri := strings.Fields(ln)[1]

			fmt.Println("The request method is : ", reqMethod)
			fmt.Println("The request URI is : ", reqUri)

		}
		if ln == "" {
			break
		}
		i++
	}
	body := "Check out this body, eh?"
	io.WriteString(c, "HTTP/1.1 200 OK\r\n")
	fmt.Fprintf(c, "Content-Length: %d\r\n", len(body))
	fmt.Fprint(c, "Content-Type: text/plain\r\n")
	io.WriteString(c, "\r\n")
	io.WriteString(c, body)
}

func main() {
	li, _ := net.Listen("tcp", ":8080")
	defer li.Close()

	for {
		conn, _ := li.Accept()
		serve(conn)
	}
}
