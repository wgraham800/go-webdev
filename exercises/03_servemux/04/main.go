package main

import (
	"io"
	"net"
)

func main() {
	li, _ := net.Listen("tcp", ":8080")
	defer li.Close()

	for {
		conn, _ := li.Accept()

		go func(c net.Conn) {
			defer c.Close()
			_, _ = io.WriteString(c, "I see you connected.")
		}(conn)
	}
}
