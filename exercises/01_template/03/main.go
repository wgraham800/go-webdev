package main

import (
  "encoding/csv"
  "time"
  "os"
  "text/template"
  "log"
  "strconv"
)

type Record struct {
  Date time.Time
  Open, High, Low, Close, Vol, Adj_close float64
}

var tpl *template.Template

func init() {
  tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {

  f, err := os.Open("table.csv")
  if err != nil {
    log.Fatalln(err)
  }


  r := csv.NewReader(f)
  rows, err := r.ReadAll()
  if err != nil {
    log.Fatalln(err)
  }

  records := make([]Record, 0, len(rows))
  for i, row := range rows {
    // header row
    if i == 0 {
      continue
    }
    date, _ := time.Parse("2006-01-02", row[0])
    open, _ := strconv.ParseFloat(row[1], 64)

    records = append(records, Record{
      Date: date,
      Open: open,
    })
  }

  err = tpl.Execute(os.Stdout, records)
  if err != nil {
    log.Fatalln(err)
  }
}
