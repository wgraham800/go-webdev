package main

import (
	"log"
	"os"
	"text/template"
)

// Route struct defines a rock climbing route. It contains the name of the route
// the grade, number of pitches, Location, and the style (trad, sport, mixed, other)
type route struct {
	Name, Style, Location string
	Grade                 float32
	Pitches               int
}

type crag struct {
	Location string
	Routes   []route
}

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
	crags := []crag{
		crag{
			Location: "Boulder Canyon",
			Routes: []route{
				route{
					Name:    "Young and Rackless",
					Grade:   5.7,
					Pitches: 4,
					Style:   "Sport",
				},
			},
		},
		crag{
			Location: "Flatirons",
			Routes: []route{
				route{
					Name:     "Freeway",
					Grade:    5.2,
					Pitches:  10,
					Style:    "Trad/Solo",
					Location: "Flatirons",
				},
				route{
					Name:     "Direct East Face (DEF)",
					Grade:    5.4,
					Pitches:  10,
					Style:    "Trad/Solo",
					Location: "Flatirons",
				},
			},
		},
		crag{
			Location: "Shelf Road - Cactus Wall",
			Routes: []route{
				route{
					Name:     "Number One Super Guy",
					Grade:    5.11,
					Pitches:  1,
					Style:    "Sport",
					Location: "Shelf Road - Cactus Wall",
				},
			},
		},
	}

	err := tpl.Execute(os.Stdout, crags)
	if err != nil {
		log.Fatalln(err)
	}
}
