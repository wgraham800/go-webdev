package main

import (
	"html/template"
	"net/http"
)

func main() {
	
	fs := http.FileServer(http.Dir("starting-files/public"))
	http.Handle("/pics/", fs)
	http.HandleFunc("/", index)
	http.ListenAndServe(":8080", nil)
}

func index(w http.ResponseWriter, req *http.Request) {
	tpl := template.Must(template.ParseFiles("starting-files/templates/index.gohtml"))
	tpl.Execute(w, nil)
}
