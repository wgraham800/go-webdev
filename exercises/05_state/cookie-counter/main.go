package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
)

func main() {
	http.HandleFunc("/", cookie)
	http.ListenAndServe(":8080", nil)
}

func cookie(w http.ResponseWriter, req *http.Request) {
	cookie, err := req.Cookie("visit-counter")

	if err == http.ErrNoCookie {
		cookie = &http.Cookie{
			Name:  "visit-counter",
			Value: "0",
			Path:  "/",
		}
	}

	visits, err := strconv.Atoi(cookie.Value)
	if err != nil {
		log.Fatal("Something real bad happened")
	}
	visits++
	cookie.Value = strconv.Itoa(visits)
	http.SetCookie(w, cookie)
	io.WriteString(w, cookie.Value)
	fmt.Println("Wrote a cookie!")
	fmt.Printf("You have visited this page %s times!\n", cookie.Value)
}
