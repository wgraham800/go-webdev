module gitlab.com/wgraham800/go-webdev

go 1.18

require github.com/google/uuid v1.3.0

require golang.org/x/crypto v0.0.0-20220722155217-630584e8d5aa // indirect
